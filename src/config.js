const config = {
    node_env: process.env.VUE_APP_NODE_ENV || 'development',
    api: 'http://jsonplaceholder.typicode.com/'
}  

export default config
