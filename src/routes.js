/*=========================================================================================
Description: Routes for vue-router. Lazy loading is enabled.

Object Strucutre:
{
  path => router path
  name => router name
  component(lazy loading) => component to load
  meta : {
    rule => which user can have access (ACL)
    breadcrumb => Add breadcrumb to specific page
    pageTitle => Display title besides breadcrumb
  }
}
==========================================================================================*/

import MainLayout from "@/views/layouts/Main.vue"
import DashboardView from "@/views/Dashboard.vue"

export default [
  {
    path: '',
    component: MainLayout,
    children: [
      {
        path: '/',
        component: DashboardView
      }
    ],
    meta: {
      requiresAuth: false
    },
  },
  {
    path: '*',
    redirect: '/',
  }
]
