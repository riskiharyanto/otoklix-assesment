
const state = {
    isAllUser: true,
    isUser: false,
    isContent: false
  }
  
  const getters = {}
  
  const actions = {}
  
  const mutations = {
    SET_ALL_USER(state, payload) {
      state.isAllUser = payload
      state.isUser = false
      state.isContent = false
    },
    SET_USER(state, payload) {
      state.isAllUser = false
      state.isUser = payload
      state.isContent = false
    },
    SET_CONTENT(state, payload) {
      state.isAllUser = false
      state.isUser = false
      state.isContent = payload
    },
  }
  
  export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
  }
  