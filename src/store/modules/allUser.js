import axios from '@/utils/axios.js'
import config from '@/config.js'

const state = {
    items: []
  }
  
  const getters = {}
  
  const actions = {
    getData({commit}){
      return new Promise((resolve, reject) => {
        axios.request({
          url: 'users',
          method: 'GET',
          baseURL: `${config.api}`
        }).then(response => {
          commit("SET_ITEMS", response.data)
          resolve(response)
        }).catch(e => {
          console.log(e)
          reject(e)
        })
      })
    },
  }
  
  const mutations = {
    SET_ITEMS(state, payload) {
      state.items = payload
    }
  }
  
  export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
  }
  