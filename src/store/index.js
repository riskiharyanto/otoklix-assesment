import Vue from 'vue'
import Vuex from 'vuex'
import dashboard from './modules/dashboard'
import allUser from './modules/allUser'
import user from './modules/user'
import content from './modules/content'
// import config from '@/config.js'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    dashboard,
    allUser,
    user,
    content
  }
})
